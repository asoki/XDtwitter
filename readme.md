I'm tired of manually changing x.com or twitter.com to vxtwitter.com to share anime pics over the internet, hence this utility to automatically replace it when copying.

Also replaces pixiv with phixiv, but pixiv seems to work fine for the most part (even if the embeds aren't great and R18 isn't allowed to embed either)

## installing dependencies
you could use your favorite virtual environment manager or just install it raw.
```
pip install -r requirements.txt
```

## running
```
python main.py
```

also removes &t and &s query params because no thanks!