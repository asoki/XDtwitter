from abc import ABC

PROGRAM_END = "PROGRAM_END"
IS_CAPTURING_TWITTER = "IS_CAPTURING_TWITTER"
IS_CAPTURING_PIXIV = "IS_CAPTURING_PIXIV"


class StateMachineMeta(ABC):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance

        return cls._instances[cls]

    def set(self, key: str, value) -> None:
        setattr(self.__class__, "".join(("_", key)), value)


class StateMachine(StateMachineMeta):
    _PROGRAM_END = False
    _IS_CAPTURING_TWITTER = True
    _IS_CAPTURING_PIXIV = True

    @property
    def PROGRAM_END(self) -> bool:
        return self._PROGRAM_END

    @property
    def IS_CAPTURING_TWITTER(self) -> bool:
        return self._IS_CAPTURING_TWITTER

    @property
    def IS_CAPTURING_PIXIV(self) -> bool:
        return self._IS_CAPTURING_PIXIV


sm = StateMachine()


def exit_program() -> None:
    sm.set(PROGRAM_END, True)


def toggle_capture_twitter() -> None:
    sm.set(IS_CAPTURING_TWITTER, not sm.IS_CAPTURING_TWITTER)


def toggle_capture_pixiv() -> None:
    sm.set(IS_CAPTURING_PIXIV, not sm.IS_CAPTURING_PIXIV)


def disable_enable_all() -> None:
    value = not (sm.IS_CAPTURING_PIXIV and sm.IS_CAPTURING_TWITTER)
    # if value:
    #     value = False if sm.IS_CAPTURING_PIXIV or sm.IS_CAPTURING_TWITTER else True
    sm.set(IS_CAPTURING_TWITTER, value)
    sm.set(IS_CAPTURING_PIXIV, value)
