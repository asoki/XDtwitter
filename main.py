# Automatically re-formats copied twitter/x links to vxtwitter because melon XD.

from contextlib import suppress
import re
import time

import pyperclip
from PIL import Image
from pystray import Icon, Menu, MenuItem

import menu

sm = menu.StateMachine()

TWTITTER_URL_MATCHER = re.compile(
    r"^https://(?:www\.)?(?:x|twitter)\.com/([a-zA-Z0-9_]+/status/\d+)"
)
PIXIV_URL_MATCHER = re.compile(r"^https://(?:www\.)?pixiv\.net/((?:en/)?artworks/\d+)")


def parse_twitter_clipboard() -> None:
    string = pyperclip.paste()
    match = TWTITTER_URL_MATCHER.match(string)
    if match:
        pyperclip.copy(f"https://vxtwitter.com/{match[1]}")


def parse_pixiv_clipboard() -> None:
    string = pyperclip.paste()
    match = PIXIV_URL_MATCHER.match(string)
    if match:
        pyperclip.copy(f"https://phixiv.net/{match[1]}")


def main():
    icon_image = Image.open("resources/xD.png")
    icon = Icon(
        "XDTwitter",
        icon=icon_image.copy(),
        menu=Menu(
            MenuItem(
                text="Disable/enable all",
                action=menu.disable_enable_all,
            ),
            MenuItem(
                text="Capture twitter",
                action=menu.toggle_capture_twitter,
                checked=lambda _: sm.IS_CAPTURING_TWITTER,
            ),
            MenuItem(
                text="Capture pixiv",
                action=menu.toggle_capture_pixiv,
                checked=lambda _: sm.IS_CAPTURING_PIXIV,
            ),
            MenuItem(
                text="Exit",
                action=menu.exit_program,
            ),
        ),
    )
    icon_image.close()
    icon.run_detached()

    while True:
        if sm.IS_CAPTURING_TWITTER:
            with suppress(pyperclip.PyperclipWindowsException):
                parse_twitter_clipboard()
        if sm.IS_CAPTURING_PIXIV:
            with suppress(pyperclip.PyperclipWindowsException):
                parse_pixiv_clipboard()

        time.sleep(0.2)

        if sm.PROGRAM_END:
            icon.stop()
            break


if __name__ == "__main__":
    main()
